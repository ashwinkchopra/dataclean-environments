# OpenCV

[OpenCV][OpenCV] is an open source computer vision and machine learning
software library. OpenCV was built to provide a common infrastructure for
computer vision applications and to accelerate the use of machine perception in
the commercial products. Being a BSD-licensed product, OpenCV makes it easy for
businesses to utilize and modify the code.

## Instructions

1. Create a new environment, and navigate to the 'System' section. Add the
   package `libgl1-mesa-dev` to the apt package list.
1. Navigate to the 'Python' section. In the 'Conda' subsection, click 'Add
   extra channel' and enter `conda-forge`.
1. In the 'Python' section, select `conda` in the second combo box, enter
   `opencv` in the package name box, and add the package.

You can now apply this environment to a server.

[OpenCV]: http://opencv.org/
