# Neovim

[Neovim][neovim] is a terminal-based text editor extending the ubiquitous Vim
text editor. Since Ubuntu does not provide a Neovim package, this environment
first adds an Ubuntu PPA which provides one.

[neovim]: https://neovim.io/
