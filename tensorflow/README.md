# TensorFlow

[TensorFlow][tensorflow] is an open source software library for numerical
computation using data flow graphs. The upstream TensorFlow package is already
installed on SherlockML servers, however this environment will build TensorFlow
from source to make use of optimisations specific to the CPU features of
SherlockML servers.

[tensorflow]: https://www.tensorflow.org/
