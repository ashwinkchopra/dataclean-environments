#!/bin/bash

set -eu

source activate Python3

pip install jupyter_contrib_nbextensions
jupyter contrib nbextension install --user
jupyter nbextension enable python-markdown/main

sudo sv restart jupyter

