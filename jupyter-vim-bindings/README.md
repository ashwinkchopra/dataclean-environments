# Jupyter vim bindings

This installs Vim bindings for Jupyter notebooks.

Jupyter notebooks provide Vim-like bindings for actions on entire cells, but
not for text editing within a cell. This SherlockML environment installs
[this](https://github.com/lambdalisue/jupyter-vim-binding) Jupyter extension
that provides Vim emulation when editing text within a cell.

To get started, install this environment and press `F1` in a notebook.
