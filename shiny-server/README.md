# Shiny Server

[Shiny][shiny] is a web application framework for [R][R]. This environment
installs RStudio Shiny Server, and configures it to be served instead of the
default Jupyter notebook on a SherlockML server.

[R]: https://www.r-project.org/
[shiny]: https://shiny.rstudio.com/
