# Jupyter Scala kernel

This environment installs a [Scala kernel][kernel] for the Jupyter notebook.

[kernel]: https://github.com/alexarchambault/jupyter-scala
