#!/bin/sh

set -eu

export DEBIAN_FRONTEND=noninteractive

sudo apt-get update -qq
sudo apt-get install -qq openjdk-8-jdk

curl -Lo coursier https://git.io/vgvpD
sudo install coursier /usr/local/bin/coursier

git clone https://github.com/alexarchambault/jupyter-scala
./jupyter-scala/jupyter-scala --force
