#!/bin/bash

set -eu

# Install Apache Airflow and initialise database.
source activate Python3
pip install --upgrade --no-deps airflow
airflow initdb

# Stop the running Jupyter notebook server, configure the init system to run
# Apache Airflow in it's place, and start the Apache Airflow service.
sudo sv stop jupyter
sudo rm -fr /etc/service/jupyter
sudo mkdir -p /etc/service/airflow
cat > run <<EOF
#!/bin/sh
set -e
exec /sbin/setuser sherlock env PATH=/opt/anaconda/envs/Python3/bin:$PATH \
    airflow webserver -p 8888
EOF
chmod 755 run
sudo mv run /etc/service/airflow/run
