# Apache Airflow

[Apache Airflow][airflow] is a platform to programmatically author, schedule
and monitor workflows as directed acyclic graphs of tasks.

After applying this environment, visit the `/admin/` path on your server to
access the Airflow web interface.

[airflow]: https://airflow.incubator.apache.org/
