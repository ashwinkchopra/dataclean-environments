# Fish

This environment installs [fish](https://fishshell.com/) and sets it to be your
login shell on a SherlockML server.

## Customisation

One of the options to make your prompt nicer and include details from tools
such as Git and Conda is to install the
[bobthefish](https://github.com/oh-my-fish/theme-bobthefish) theme using the
[oh-my-fish](https://github.com/oh-my-fish/oh-my-fish) package manager.

In your fish shell, run:

```bash
curl -L https://get.oh-my.fish | fish
omf install bobthefish
```

Add the following line

```fish
set -g theme_powerline_fonts no
```

to `~/.config/fish/config.fish` to disable the non-standard fonts required by
the theme by default. You have to start a new fish session to see the font
settings applied.

Because your home directory is persisted across all SherlockML servers, your
custom configuration will be present on other servers your create.

The result will look like this:
![preview](preview.gif)
