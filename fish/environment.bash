#!/bin/bash

# Append a line to fish config if not present
add_to_config() {
   VALUE=$1
   FILE=$HOME/.config/fish/config.fish
   mkdir -p "$(dirname "$FILE")"
   touch "$FILE"
   grep --quiet "$VALUE" "$FILE" || echo "$VALUE" >> "$FILE"
}

export DEBIAN_FRONTEND=noninteractive

sudo apt-add-repository -y ppa:fish-shell/release-2
sudo apt-get update -qq
sudo apt-get install -qq fish

# Change default shell to fish
chsh -s "$(which fish)" sherlock

# Copy bash environment variables to fish config
grep -E -v '^export CUBE_........_.' /etc/profile.d/container_environment.sh |
    grep -v '^export PATH' | sudo tee /etc/profile.d/container_environment.fish > /dev/null
add_to_config "source /etc/profile.d/container_environment.fish"

# Fix conda path
CONDA_ROOT=$(conda info --root)
add_to_config "set -x PATH $CONDA_ROOT/bin \$PATH"
add_to_config "source $CONDA_ROOT/etc/fish/conf.d/conda.fish"
