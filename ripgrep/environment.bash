#!/bin/bash

# Unfortunately, upstream does not provide an Ubuntu package, so we instead
# download a Linux binary statically linked against musl libc.

RIPGREP_VERSION=0.5.2

# Remove any previous installation in case of upgrade.
sudo rm -fr /opt/ripgrep

wget https://github.com/BurntSushi/ripgrep/releases/download/$RIPGREP_VERSION/ripgrep-$RIPGREP_VERSION-x86_64-unknown-linux-musl.tar.gz

tar xvf ripgrep-$RIPGREP_VERSION-x86_64-unknown-linux-musl.tar.gz

rm ripgrep-$RIPGREP_VERSION-x86_64-unknown-linux-musl.tar.gz

sudo mv ripgrep-$RIPGREP_VERSION-x86_64-unknown-linux-musl /opt/ripgrep

sudo install -D /opt/ripgrep/rg /usr/local/bin/rg
sudo install -m 644 -D /opt/ripgrep/rg.1 /usr/local/share/man/man1/rg.1
