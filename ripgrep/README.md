# ripgrep

[ripgrep][ripgrep] is a line oriented search tool that combines the usability
of [The Silver Searcher][ag] (similar to [ack][ack]) with the raw speed of [GNU
Grep][grep]. ripgrep works by recursively searching your current directory for
a regex pattern.

[ack]: https://beyondgrep.com/
[ag]: https://geoff.greer.fm/ag/
[grep]: https://www.gnu.org/software/grep/
[ripgrep]: https://github.com/BurntSushi/ripgrep
